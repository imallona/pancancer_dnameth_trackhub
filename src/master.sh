#!/bin/bash


usage(){

    cat << EOF

Usage:

Specify either working directory or output directory or both options.
Working directory must contain necessary files.

OPTIONS:
	-h Show this message
	-d Working directory (default: current directory)
	-o Output directory (default: located in working directory)

EOF
}


workdir=""
outputdir=""
tmpdir=""
tissues=""
namecheck=""
CORE_FILES=(cg_widen.R gene_names_intervals.R percentage.R meth_analysis.R generate_trackdb_tracks.R database.R db.txt annot_datasets.txt hg19.chrom.sizes bed.as bed_stats.as)
BEDTOOLS=/soft/bio/bedtools2-2.25.0/bin
LETTERS=(e d c b a)

while getopts “:hd:o:” OPTION
do
    case $OPTION in
        h)
            usage
            exit 0
            ;;
        d)
            workdir=$OPTARG
            ;;
        o)
            outputdir=$OPTARG
            ;;
        ?)
        usage
        exit 1
        ;;
    esac
done

if [[ -n "$workdir" ]] || [[ -n "$outputdir" ]]; then

    if [ ${#workdir} -ne 0 ]; then

	if [ ! -d "$workdir" ]; then

	    echo "Error: Working directory '$workdir' does not exist."
	    usage
	    exit 1

	else

	    echo "Working directory set in $workdir"

	fi

    else

	workdir=$(pwd)
	echo "Working directory option not specified; default working directory set in the current directory $workdir"

    fi

    for file in ${CORE_FILES[*]}    # check core files
    do
	if [ ! -f "${workdir}/${file}" ]; then
	    echo "Missing file: $file"
	    usage
	    exit 1
	fi
    done



    if [ ${#outputdir} -ne 0 ]; then

	if [ ! -d "$outputdir" ]; then

	    read -p "Output directory does not exist, create $outputdir directory?(y or n) " -n 1 -r
	    echo

	    if [[ $REPLY =~ ^[Yy]$ ]]; then

		mkdir -p "$outputdir"
		echo "$outputdir directory created"

	    else
		echo "Operation cancelled"
		exit
	    fi

	else

	    echo "Output directory set in $outputdir"

	fi

    else

	read -p "Output option not specified; by default this will create an output folder in the current working directory $workdir , continue?(y or n) " -n 1 -r
	echo

	if [[ $REPLY =~ ^[Yy]$ ]]; then

	    mkdir "${workdir}/Output_folder"
	    outputdir="${workdir}/Output_folder"
	    echo "$outputdir directory created"

	else

	    echo "Operation cancelled"
	    exit

	fi
    fi

else
    usage
    exit 1
fi

mkdir "${workdir}/tmp_files"
tmpdir="${workdir}/tmp_files"

echo "Checking Bedtools version.."

if [ ! -d "$BEDTOOLS" ]; then

    echo "Incorrect version: bedtools2-2.25.0 required"
    exit 1

fi

cd "$workdir"

#### Fetches cg intervals from the annotations database; widen intervals; writes into files (Bed format with header).

for chr in {22..1} X Y
do

    Rscript cg_widen.R "$chr" "$workdir" "$tmpdir"

done

## Got one file for each chromosome




#### All in one; concatenates previous files
## Requires an empty file

> "${tmpdir}/all_cg_widen_intervals.bed"
for chr in {1..22} X Y
do
    cat "${tmpdir}/all_cg_widen_intervals.bed" "${tmpdir}/chr${chr}_cg_widen_intervals.bed" > "${tmpdir}/tmp"
    mv "${tmpdir}/tmp" "${tmpdir}/all_cg_widen_intervals.bed"
done

#### Got one file with all the intervals
## Caution: Multiple headers!!

awk '{if (NR != 1 && $1 ~ /probe/) {next} {OFS=FS="\t"; print}}' "${tmpdir}/all_cg_widen_intervals.bed" > "${tmpdir}/tmp"
mv "${tmpdir}/tmp" "${tmpdir}/all_cg_widen_intervals.bed"

##


#### Fetches gene names and gene intervals from annotations database

Rscript gene_names_intervals.R "$workdir" "$tmpdir"

#### Got gene names and gene intervals for each chromosome




#### Find the closest gene for each cg interval
## Requires bedtools2

> "${tmpdir}/all_distances.txt"
for chr in {1..22} X Y
do
    awk '{ if (NR !=1) {OFS=FS="\t"; print $2, $3 ,$4}}' "${tmpdir}/chr${chr}_cg_widen_intervals.bed"\
	| "${BEDTOOLS}/closestBed" -d -t first -a stdin -b "${tmpdir}/chr${chr}_gene_intervals.bed" \
	| cat "${tmpdir}/all_distances.txt" - > "${tmpdir}/tmp_distances"
    mv "${tmpdir}/tmp_distances" "${tmpdir}/all_distances.txt"
done

#### Got a file with all the distances



####################### Ask for chr and tissue?

> "${outputdir}/trackDb.txt"

# for dataset in $(cut -f1 annot_datasets_reduced.txt)
while read line
do

    COUNT=1
    dataset=$(echo "$line" | cut -f1)
    name=$(echo "$line" | cut -f2)
    lname=$(echo "$line" | cut -f3)
    echo name is $name
    echo dataset is $dataset

    #### Calculates percentage of pacients within each methylation value interval and tissue type
    ## Requires the previous intervals file

    Rscript percentage.R "$workdir" "$tmpdir" all_cg_widen_intervals.bed "$dataset"

    #### Got one Bed file for each interval and tissue type


#### Adding genename, distance and link columns + permille score


    #for name in $(cut -f2 annot_datasets_reduced.txt)    #do
    for i in 0.2 0.4 0.6 0.8 1.0
    do
		sort -k4,4 "${tmpdir}/${dataset}_${i}_percentage.bed" \
		| join -a1 -1 4 -2 1 -o 1.1 1.2 1.3 0 1.5 2.2 -t $'\t' - "${tmpdir}/probe_associated_genes.txt" \
		| sort -k1,1 -k2,2n - > "${tmpdir}/${dataset}_${i}_permille.bed"

                echo 'Uncomment to introduce link to wanderer and dist to gene'
        # awk '{OFS=FS="\t"; print $4, $5, $6, $7}' "${tmpdir}/all_distances.txt" \
	# 	| "${BEDTOOLS}/slopBed" -b 100000 -i - -g hg19.chrom.sizes \
	# 	| paste "${tmpdir}/${dataset}_${i}_permille.bed" - \
	# 	| awk -v var="$name" '{OFS=FS="\t"; if ($10 > 100000) {print $1, $2, $3, $4, int($5)*10, $6, $10, "Too far from the gene"} else {printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t<a href=\"http://www.maplab.cat/wanderer_api?Gene=%s&start=%s&end=%s&TissueType=%s&DataType=methylation&plotmean=FALSE&geneLine=TRUE&CpGi=TRUE&nN=30&nT=30&distribute_uniformly=FALSE&pvalThres=0.05\" > Wanderer </a>\n", $1, $2, $3, $4, int($5)*10, $6, $10, $6, $8, $9, var}}' - > "${tmpdir}/tmp.bed"

	# 	mv "${tmpdir}/tmp.bed" "${tmpdir}/${dataset}_${i}_permille.bed"

		echo "Done for ${dataset}_${i}_permille.bed"

                ## int rounding and increasing score 10x (ranges 0 to 1000)
                awk '{OFS=FS="\t"; print $1,$2,$3,$4,int($5 * 10),$6}' "${tmpdir}/${dataset}_${i}_permille.bed" > foo
                mv foo "${tmpdir}/${dataset}_${i}_permille.bed"

		bedToBigBed -type=bed5+1 -as=bed_5_plus_1.as -tab "${tmpdir}/${dataset}_${i}_permille.bed" hg19.chrom.sizes "${outputdir}/${dataset}_${i}_permille.bb"

		Rscript generate_trackdb_tracks.R "$dataset" "$i" trackDb.txt "$COUNT" "$lname" "$namecheck" "${LETTERS[$(($COUNT - 1))]}" "$outputdir"

		COUNT=$(($COUNT+1))

    done


    echo 'Uncomment to calculate stats'
    # #### Calculates methylation statistics between normal and tumor tissues

    # if [ $name == $namecheck ]; then

    #     	Rscript meth_analysis.R "$workdir" "$tmpdir" all_cg_widen_intervals.bed "$name"

    #     	"${BEDTOOLS}/sortBed" -i "${tmpdir}/${name}_stats.bed" > "${tmpdir}/tmp2.bed"
    #     	mv "${tmpdir}/tmp2.bed" "${tmpdir}/${name}_stats.bed"

    #     	#### Generate bigBeds for statistics bed files
    #     	bedToBigBed -type=bed9+3 -as=bed_stats.as -tab "${tmpdir}/${name}_stats.bed" hg19.chrom.sizes "${outputdir}/${name}_stats.bb"
	
    #     	echo "Done with $name stats"
	
    # else

    #     	namecheck=$name

    # fi

	
done < annot_datasets.txt
####

echo 'Remember to remove $tmpdir'
echo $tmpdir
# rm -rf "$tmpdir"



