#!/usr/bin/env R
#
## Widen methilation probes for bed files
##
## Alberto Sierco, 12th Aug 2015

### Check scipen (scientific notation)
options(scipen = 999)

library(RPostgreSQL)

args <- commandArgs(trailingOnly = TRUE)

chromArg <- args[1]

workDir <- args[2]

tmpDir <- args[3]

source(file.path(workDir, 'database.R'))

DB_CONF <- file.path(workDir, 'db.txt')

drv <- dbDriver('PostgreSQL')

con <- db_connect(DB_CONF)

kStep <- 10

Usage <- function(x) {
  cat('\nUSAGE:\n')
  cat('  Rscript cg_widen.R <nºchrom>\n')
  cat('Example:\n')
  cat('  Rscript cg_widen.R 12\n')
}


if (length(args) != 3) {
    Usage()
    stop(sprintf('Malformed query, you entered %s arguments and only two are needed', length(args)))
}

print('Working on ')
print(chromArg)


annot <- get_query(con, sprintf("SELECT * FROM annotations.humanmethylation450kannot WHERE chr='chr%s' AND probetype='cg' order by chr, cg_start, cg_end;", chromArg))

## to standardize locations
print('Uncomment to standardize locations')
## annot$cg_start <- annot$cg_start - 1

# This data frame will storage new start/end data
new_cg_intervals <- data.frame(probe = annot$probe,
        chr = annot$chr,
	    widen_cg_start = rep(NA, nrow(annot)),
		widen_cg_end = rep(NA, nrow(annot)))

# Assign the values for the ends of the data
new_cg_intervals[1,]$widen_cg_start <- annot[1, 'cg_start'] - kStep
new_cg_intervals[nrow(new_cg_intervals),]$widen_cg_end <- annot[nrow(new_cg_intervals), 'cg_end'] + kStep

for (i in 1:nrow(new_cg_intervals)){
	if (i%%100 == 0){
        cat(sprintf('...%s processed...', i))
    }
   	# chrom_prev <- annot[i, 'chr']
	# chrom <- annot[i+1, 'chr']
	end_prev <- annot[i, 'cg_end'] + kStep
	start <- annot[i+1, 'cg_start']
	ct <- 0
	# if (chrom_prev == chrom){
		while (ct == 0 && i < nrow(new_cg_intervals) ){
			if (end_prev < start){
				if (end_prev < start - kStep){
					start <- start - kStep
					new_cg_intervals[i,]$widen_cg_end <- end_prev
					new_cg_intervals[i+1,]$widen_cg_start <- start
					ct <- 1
				}
				else
					start <- end_prev + 1
					new_cg_intervals[i,]$widen_cg_end <- end_prev
					new_cg_intervals[i+1,]$widen_cg_start <- start
					ct <- 1
			}
			else
				end_prev <- start - 1
				new_cg_intervals[i,]$widen_cg_end <- end_prev
				new_cg_intervals[i+1,]$widen_cg_start <- start
				ct <- 1
		}


	# }
}

write.table(new_cg_intervals,
                file = file.path(tmpDir, sprintf("chr%s_cg_widen_intervals.bed", chromArg)),
                quote = FALSE, col.names = TRUE, row.names = FALSE, sep = "\t")




