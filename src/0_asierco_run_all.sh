#!/bin/bash
# request Bourne shell as shell for job
#$ -S /bin/bash
# Name for the script in the queuing system
#$ -N asierco_all
# name of the queue you want to use
#$ -q imppc
# In order to load the environment variables and your path
# You can either use this or do a : source /etc/profile
#$ -V
# You can redirect the error output to a specific file
#$ -e  /imppc/labs/maplab/share/imallona2asierco/all-e.log
# You can redirect the output to a specific file
#$ -o  /imppc/labs/maplab/share/imallona2asierco/all-o.log
# In order to receive an e-mail at the '''b'''egin of the execution and in the '''e'''nd of it
#$ -m be
# You have to specify an address
#$ -M imallona@imppc.org
 
# Actual work
  
# print date, time and hostname
echo Start at $(date)

cd $ISHARE/imallona2asierco/trackhubber

# head $ISHARE/asierco2izaskun/current/meth_analysis.R

# /soft/bin/Rscript $ISHARE/asierco2izaskun/current/meth_analysis.R \
#         . chr22_cg_widen_intervals.bed luad

mkdir -p results
bash master.sh -d . -o results

echo End at $(date)
