table hg19
"CpG methylation"
(
string  chrom;		"Reference sequence chromosome"
uint    chromStart;	"Start position of feature on chromosome"
uint    chromEnd;	"End position of feature on chromosome"
string  name;	    "Name of probe"
uint    score;		"Score"
char[1]	strand;		"strand"
uint	thickStart;	"thickStart"
uint	thickEnd;	"thickEnd"
uint	reserved;	"Color"
string	pvalue;		"p Value"
string  	Statistic;	"W Statistic"
string    deltaMean;   "Tumor vs Normal mean methylation difference"
)

