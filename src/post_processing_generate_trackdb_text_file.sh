#!/bin/bash

outputdir=$1
namecheck=""
LETTERS=(e d c b a)

# for dataset in $(cut -f1 annot_datasets_reduced.txt)
while read line
do
    # echo $line

    COUNT=1
    dataset=$(echo "$line" | cut -f1)
    name=$(echo "$line" | cut -f2)
    lname=$(echo "$line" | cut -f3)
	
        for i in 0.2 0.4 0.6 0.8 1.0
        do
	
	
                Rscript generate_trackdb_tracks.R "$dataset" "$i" trackDb.txt "$COUNT" "$lname" "$namecheck" "${LETTERS[$(($COUNT - 1))]}" "$outputdir"

        	COUNT=$(($COUNT+1))
	
        done
	
        if [ $name != $namecheck ]; then
		
        	namecheck=$name
	
        fi
	
done < annot_datasets.txt
	
