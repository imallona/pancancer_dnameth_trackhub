table hg19
"CpG methylation density"
(
string  chrom;		"Chromosome"
uint    chromStart;	"Start"
uint    chromEnd;	"End"
string  name;	    "Probe name"
uint    score;		"Score"
string  genename;	"Closest gene symbol"
)

